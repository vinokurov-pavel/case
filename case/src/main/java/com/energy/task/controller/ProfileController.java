package com.energy.task.controller;

import com.energy.task.model.Profile;
import com.energy.task.service.DataValidationException;
import com.energy.task.service.ProfileService;
import com.energy.task.transport.ProfileTO;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.convert.ConversionService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Controller to provide REST methods for Profile entity.
 */
@RestController("profileController")
@RequestMapping("api/profile")
@Api(value = "profile", description = "Profile CRUD operations")
public class ProfileController {

	private static final Logger LOGGER = LoggerFactory.getLogger(ProfileController.class);

	@Autowired
	@Qualifier("conversionService")
	private ConversionService conversionService;

	@Autowired
	@Qualifier("profileService")
	private ProfileService profileService;


	@ExceptionHandler({DataValidationException.class})
	@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "Invalid data")
	public void handleServiceException(DataValidationException ex) {
		LOGGER.warn("Handle validation exception", ex.getMessage());
	}

	@ApiOperation(value = "Save a profile")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successfully saved profile"),
			@ApiResponse(code = 400, message = "Invalid data"),
	}
	)
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public void save(@RequestBody ProfileTO profileTO) {
		Profile profile = conversionService.convert(profileTO, Profile.class);
		profileService.save(profile);
	}

	@ApiOperation(value = "Retrieve profile by profile name")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Profile successfully retrieved"),
			@ApiResponse(code = 404, message = "Profile not found"),
	}
	)
	@ApiParam(value = "Profile name", required = true, example = "A")
	@RequestMapping(value = "/get/{profile}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody
	ResponseEntity<ProfileTO> get(@PathVariable("profile") String profileName) {
		Profile profile = profileService.find(profileName);
		if (profile == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		ProfileTO profileTO = conversionService.convert(profile, ProfileTO.class);
		return new ResponseEntity<>(profileTO, HttpStatus.OK);
	}

	@ApiOperation(value = "Delete profile by name")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Profile successfully deleted"),
	}
	)
	@ApiParam(value = "Profile name", required = true, example = "A")
	@RequestMapping(value = "/delete/{profile}", method = RequestMethod.POST)
	public void delete(@PathVariable("profile") String profileName) {
		profileService.delete(profileName);
	}

}
