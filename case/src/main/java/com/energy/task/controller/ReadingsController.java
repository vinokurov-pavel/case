package com.energy.task.controller;

import com.energy.task.model.Readings;
import com.energy.task.service.ReadingsService;
import com.energy.task.transport.ReadingsTO;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.convert.ConversionService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Controller to provide REST methods for Meter readings
 */
@RestController("readingsController")
@RequestMapping("api/readings")
@Api(value = "readings", description = "Readings CRUD operations")
public class ReadingsController {

	private ReadingsService readingsService;

	private ConversionService conversionService;

	@Autowired
	public ReadingsController(@Qualifier("conversionService") ConversionService conversionService, @Qualifier("readingsService") ReadingsService readingsService) {
		this.conversionService = conversionService;
		this.readingsService = readingsService;
	}


	@ApiOperation(value = "Save a metric readings")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Readings successfully saved"),
	}
	)
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public void save(@RequestBody ReadingsTO readingsTO) {
		Readings readings = conversionService.convert(readingsTO, Readings.class);
		readingsService.save(readings);
	}

	@ApiOperation(value = "Retrieve readings by connectionId")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Profile successfully retrieved"),
			@ApiResponse(code = 404, message = "Profile not found"),
	}
	)
	@RequestMapping(value = "/get/{connectionId}", method = RequestMethod.GET, produces = "application/json")
	@ApiParam(value = "Connection Id", required = true, example = "0001")
	public @ResponseBody
	ResponseEntity<ReadingsTO> get(@PathVariable("connectionId") String connectionId) {
		Readings readings = readingsService.find(connectionId);
		if (readings == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(conversionService.convert(readings, ReadingsTO.class), HttpStatus.OK);
	}

	@ApiOperation(value = "Delete readings by connectionId")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Readings successfully deleted"),
	}
	)
	@ApiParam(value = "Connection Id", required = true, example = "0001")
	@RequestMapping(value = "/delete/{connectionId}", method = RequestMethod.POST)
	public void delete(@PathVariable("connectionId") String connectionId) {
		readingsService.delete(connectionId);
	}

}