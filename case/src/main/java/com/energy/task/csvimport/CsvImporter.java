package com.energy.task.csvimport;

import java.io.File;
import java.io.IOException;

/**
 * Service for import data from csv in legacy format.
 */
public interface CsvImporter {

	/**
	 * Import profile data from specified file.
	 *
	 * @param file csv file
	 * @throws IOException
	 */
	void importProfileData(File file) throws IOException;

	/**
	 * Import reading data from specified file.
	 *
	 * @param file csv file
	 * @throws IOException
	 */
	void importReadingData(File file) throws IOException;
}
