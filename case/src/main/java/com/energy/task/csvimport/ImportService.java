package com.energy.task.csvimport;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.FileNotFoundException;

/**
 * Service to check folder and import data from csv files.
 */
@Service("importService")
public class ImportService {

	private static final Logger LOGGER = LoggerFactory.getLogger(ImportService.class);

	private final CsvImporter csvImporter;

	@Value("${input.folder}")
	protected String folderName;

	@Value("${profile.prefix}")
	protected String profilePrefix;

	@Value("${reading.prefix}")
	protected String readingPrefix;

	@Autowired
	public ImportService(@Qualifier("csvImporter") CsvImporter csvImporter) {
		this.csvImporter = csvImporter;
	}


	@Scheduled(fixedDelayString = "${interval}")
	public void importFromFolder() throws FileNotFoundException {
		if(StringUtils.isEmpty(folderName)){
			return;
		}
		LOGGER.info("Starting check folder for new csv files");
		File folder = ResourceUtils.getFile(folderName);
		if (!folder.exists() || !folder.isDirectory()) {
			LOGGER.warn("Folder '{}' does not exist", folderName);
			return;
		}
		File[] files = folder.listFiles();

		for (File file : files) {
			if (file.isDirectory()) {
				continue;
			}
			String name = file.getName();
			if (name.startsWith(profilePrefix)) {
				try {
					csvImporter.importProfileData(file);
					//file.delete();
				} catch (Exception e) {
					LOGGER.error("Cant't import profile data from file {}", name, e);
				}
			}

			if (name.startsWith(readingPrefix)) {
				try {
					csvImporter.importReadingData(file);
					//file.delete();
				} catch (Exception e) {
					LOGGER.error("Cant't import readings data from file {}", name, e);
				}
			}
		}
	}
}
