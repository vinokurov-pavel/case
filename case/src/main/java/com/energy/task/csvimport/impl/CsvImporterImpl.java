package com.energy.task.csvimport.impl;

import com.energy.task.csvimport.CsvImporter;
import com.energy.task.csvimport.model.FractionRecord;
import com.energy.task.csvimport.model.ReadingRecord;
import com.energy.task.model.Month;
import com.energy.task.model.Profile;
import com.energy.task.model.Readings;
import com.energy.task.service.ProfileService;
import com.energy.task.service.ReadingsService;
import org.apache.commons.collections4.IterableUtils;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * Implementation of {@link CsvImporter}
 */
@Service("csvImporter")
@Transactional
public class CsvImporterImpl implements CsvImporter {

	private static final Logger LOGGER = LoggerFactory.getLogger(CsvImporterImpl.class);

	private final ProfileService profileService;

	private final ReadingsService readingsService;

	@Autowired
	public CsvImporterImpl(@Qualifier("profileService") ProfileService profileService, @Qualifier("readingsService") ReadingsService readingsService) {
		this.profileService = profileService;
		this.readingsService = readingsService;
	}

	@Override
	public void importProfileData(File file) throws IOException {
		LOGGER.info("Importing profile data from {}", file.getAbsolutePath());

		Reader in = new FileReader(file);
		Iterable<CSVRecord> records = CSVFormat.DEFAULT.parse(in);


		List<FractionRecord> fractions = new ArrayList<>();
		IterableUtils.toList(records).stream().skip(1).forEach(record -> {
			String month = record.get(0);
			String profile = record.get(1);
			Double fraction = Double.valueOf(record.get(2));
			fractions.add(new FractionRecord(profile, month, fraction));
		});

		Map<String, List<FractionRecord>> map = fractions.stream().collect(Collectors.groupingBy(FractionRecord::getProfile));
		map.forEach((name, fractionRecords) -> {
			double[] values = fractionRecords.stream()
					.sorted((o1, o2) -> Month.compare(o1.getMonth(), o2.getMonth()))
					.mapToDouble(FractionRecord::getFraction)
					.toArray();
			Profile profile = new Profile(name, Arrays.stream(values).boxed().collect(Collectors.toList()));
			profileService.save(profile);
		});
	}

	@Override
	public void importReadingData(File file) throws IOException {
		LOGGER.info("Importing readings data from {}", file.getAbsolutePath());
		Reader in = new FileReader(file);
		Iterable<CSVRecord> records = CSVFormat.DEFAULT.withSkipHeaderRecord(true).parse(in);

		List<ReadingRecord> readingRecords = new ArrayList<>();
		IterableUtils.toList(records).stream()
				.skip(1)
				.forEach(record -> {
					String connectionId = record.get(0);
					String profile = record.get(1);
					String month = record.get(2);
					Integer value = Integer.valueOf(record.get(3));
					readingRecords.add(new ReadingRecord(connectionId, profile, month, value));
				});

		Map<String, List<ReadingRecord>> map = readingRecords.stream().collect(Collectors.groupingBy(ReadingRecord::getConnectionId));
		map.forEach((connId, readingList) -> {
			int[] values = readingList.stream()
					.sorted((o1, o2) -> Month.compare(o1.getMonth(), o2.getMonth()))
					.mapToInt(ReadingRecord::getValue)
					.toArray();
			Readings readings = new Readings(connId, readingList.get(0).getProfile(), Arrays.stream(values).boxed().collect(Collectors.toList()));
			readingsService.save(readings);
		});
	}
}
