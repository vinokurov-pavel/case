package com.energy.task.csvimport.model;

/**
 * Fraction record in csv file
 */
public class FractionRecord {

	/**
	 * Profile name
	 */
	private String profile;

	/**
	 * Month
	 */
	private String month;

	/**
	 * Fraction value
	 */
	private Double fraction;

	public FractionRecord() {
	}

	public FractionRecord(String profile, String month, Double fraction) {
		this.profile = profile;
		this.month = month;
		this.fraction = fraction;
	}

	public String getProfile() {
		return profile;
	}

	public void setProfile(String profile) {
		this.profile = profile;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public Double getFraction() {
		return fraction;
	}

	public void setFraction(Double fraction) {
		this.fraction = fraction;
	}
}
