package com.energy.task.csvimport.model;

/**
 * Reading record in csv file
 */
public class ReadingRecord {

	/**
	 * Connection ID
	 */
	private String connectionId;

	/**
	 * Profile name
	 */
	private String profile;

	/**
	 * Month
	 */
	private String month;

	/**
	 * Reading value
	 */
	private Integer value;

	public ReadingRecord() {
	}

	public ReadingRecord(String connectionId, String profile, String month, Integer value) {
		this.connectionId = connectionId;
		this.profile = profile;
		this.month = month;
		this.value = value;
	}

	public String getConnectionId() {
		return connectionId;
	}

	public void setConnectionId(String connectionId) {
		this.connectionId = connectionId;
	}

	public String getProfile() {
		return profile;
	}

	public void setProfile(String profile) {
		this.profile = profile;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public Integer getValue() {
		return value;
	}

	public void setValue(Integer value) {
		this.value = value;
	}
}
