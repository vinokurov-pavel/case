package com.energy.task.service;

import com.energy.task.model.Readings;

/**
 * Service for operations with {@link Readings}
 */
public interface ReadingsService {

	/**
	 * Saves readings, if one is valid
	 *
	 * @param readings meter readings
	 */
	void save(Readings readings);

	/**
	 * Finds readings by connectionId
	 *
	 * @param connectionId connectionId
	 * @return readings
	 */
	Readings find(String connectionId);

	/**
	 * Deletes readings by connectionId
	 *
	 * @param connectionId connectionId
	 */
	void delete(String connectionId);
}
