package com.energy.task.service.impl;

import com.energy.task.model.Month;
import com.energy.task.model.Readings;
import com.energy.task.service.ConsumptionService;
import com.energy.task.service.ReadingsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Implementation of {@link ConsumptionService}
 */
@Service("consumptionService")
public class ConsumptionServiceImpl implements ConsumptionService{

	private ReadingsService readingsService;

	@Autowired
	public ConsumptionServiceImpl(@Qualifier("readingsService") ReadingsService readingsService) {
		this.readingsService = readingsService;
	}

	@Override
	public Integer calcConsumption(String connectionId, String month) {
		Readings readings = readingsService.find(connectionId);
		if(readings==null){
			return null;
		}
		int order = Month.valueOf(month).getOrder();
		List<Integer> values = readings.getValues();
		return values.get(order)-(order>0 ? values.get(order-1):0);
	}
}
