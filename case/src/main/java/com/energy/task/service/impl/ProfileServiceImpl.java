package com.energy.task.service.impl;

import com.energy.task.dao.ProfileRepository;
import com.energy.task.model.Profile;
import com.energy.task.service.DataValidationException;
import com.energy.task.service.ProfileService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

/**
 * Implementation of {@link ProfileService}
 */
@Service("profileService")
@Transactional
public class ProfileServiceImpl implements ProfileService {

	private static final Logger LOGGER = LoggerFactory.getLogger(ProfileServiceImpl.class);

	private static final double SUM_OF_FRACTIONS = 1.0;

	private final ProfileRepository profileRepository;

	@Autowired
	public ProfileServiceImpl(@Qualifier("profileRepository") ProfileRepository profileRepository) {
		this.profileRepository = profileRepository;
	}

	@Override
	public void save(Profile profile) {
		LOGGER.info("Saving profile {}", profile);
		Assert.notNull(profile, "Argument 'profile' can't be null");

		double sum = profile.getFractions().stream().mapToDouble(value -> value).sum();

		if (Double.compare(SUM_OF_FRACTIONS, sum) != 0) {
			throw new DataValidationException("Invalid fractions sum '" + sum + "'");
		}
		profileRepository.save(profile);
		LOGGER.info("Profile {} saved", profile);
	}

	@Override
	public Profile find(String profileName) {
		return profileRepository.findOne(profileName);
	}

	@Override
	public void delete(String profileName) {
		profileRepository.delete(profileName);
	}


}
