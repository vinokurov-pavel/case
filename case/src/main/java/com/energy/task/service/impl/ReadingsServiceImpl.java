package com.energy.task.service.impl;

import com.energy.task.dao.ReadingsRepository;
import com.energy.task.model.Readings;
import com.energy.task.service.ReadingsService;
import com.energy.task.service.validation.ReadingValidator;
import com.energy.task.service.validation.ValidationResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * The implementation of {@link ReadingsService}
 */
@Service("readingsService")
@Transactional
public class ReadingsServiceImpl implements ReadingsService {

	private static final Logger LOGGER = LoggerFactory.getLogger(ReadingsServiceImpl.class);

	private final ReadingsRepository readingsRepository;

	private final ReadingValidator readingValidator;

	@Autowired
	public ReadingsServiceImpl(@Qualifier("readingsRepository") ReadingsRepository readingsRepository, @Qualifier("readingValidator") ReadingValidator readingValidator) {
		this.readingsRepository = readingsRepository;
		this.readingValidator = readingValidator;
	}


	@Override
	public void save(Readings readings) {
		LOGGER.info("Saving readings {}", readings);

		ValidationResult result = readingValidator.validate(readings);

		if (result.isValid()) {
			readingsRepository.save(readings);
			LOGGER.info("Readings {} saved", readings);
		} else {
			LOGGER.warn("Readings {} is invalid. Validation result:{}", readings, result);
		}
	}

	@Override
	public Readings find(String connectionId) {
		return readingsRepository.findOne(connectionId);
	}

	@Override
	public void delete(String connectionId) {
		readingsRepository.delete(connectionId);
	}


}
