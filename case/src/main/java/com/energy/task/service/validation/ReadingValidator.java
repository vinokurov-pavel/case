package com.energy.task.service.validation;

import com.energy.task.model.Readings;

/**
 * Validator of a {@link Readings}
 */
public interface ReadingValidator {

	/**
	 * Validates the collection of readings for the single profile
	 *
	 * @param readings meter readings
	 * @return validation result
	 */
	ValidationResult validate(Readings readings);
}
