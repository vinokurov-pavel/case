package com.energy.task.service.validation;

/**
 * Validation result for a meter readings
 */
public class ValidationResult {

	/**
	 * Flag whether meter readings is valid
	 */
	private boolean valid;
	/**
	 * Error
	 */
	private ValidationError error;

	private ValidationResult() {
		this.valid = true;
	}

	private ValidationResult(ValidationError error) {
		this.valid = false;
		this.error = error;
	}

	public static ValidationResult valid() {
		return new ValidationResult();
	}

	public static ValidationResult invalid(ValidationError error) {
		return new ValidationResult(error);
	}

	public ValidationError getError() {
		return error;
	}

	public boolean isValid() {
		return valid;
	}

	@Override
	public String toString() {
		return "ValidationResult{" +
				"valid=" + valid +
				", error=" + error +
				'}';
	}

	/**
	 * Validation error type
	 */
	public enum ValidationError {
		INVALID_READING,
		PROFILE_NOT_FOUND,
		INVALID_CONSUMPTION
	}
}
