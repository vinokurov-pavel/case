package com.energy.task.service.validation.impl;

import com.energy.task.model.Profile;
import com.energy.task.model.Readings;
import com.energy.task.service.ProfileService;
import com.energy.task.service.validation.ReadingValidator;
import com.energy.task.service.validation.ValidationResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.List;

import static com.energy.task.service.validation.ValidationResult.*;

/**
 * Implementation of the {@link ReadingValidator}
 */
@Service("readingValidator")
public class ReadingValidatorImpl implements ReadingValidator {

	protected final ProfileService profileService;

	@Value("${tolerance}")
	protected double tolerance;

	@Autowired
	public ReadingValidatorImpl(@Qualifier("profileService") ProfileService profileService) {
		this.profileService = profileService;
	}

	@Override
	public ValidationResult validate(Readings readings) {
		Assert.notNull(readings, "Argument 'readings' can't be null");

		List<Integer> values = readings.getValues();
		long totalConsumption = values.get(values.size() - 1);

		Profile profile = profileService.find(readings.getProfile());
		if (profile == null) {
			return invalid(ValidationError.PROFILE_NOT_FOUND);
		}

		for (int i = 0; i < values.size(); i++) {
			int consumption = values.get(i) - (i > 0 ? values.get(i - 1) : 0);

			if (consumption < 0) {
				return invalid(ValidationError.INVALID_READING);
			}
			double expectedConsumption = profile.getFractions().get(i) * totalConsumption;
			double expectedMin = expectedConsumption * (1 - tolerance);
			double expectedMax = expectedConsumption * (1 + tolerance);
			if (consumption < expectedMin || consumption > expectedMax) {
				return invalid(ValidationError.INVALID_CONSUMPTION);
			}
		}
		return valid();
	}


}
