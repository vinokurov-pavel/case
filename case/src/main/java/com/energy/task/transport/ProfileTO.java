package com.energy.task.transport;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;

/**
 * Model to transfer a profile using REST api
 */
@ApiModel(description = "Profile with fractions")
public class ProfileTO {

	@ApiModelProperty(value = "The profile name", required = true, example = "A")
	private String profile;

	@ApiModelProperty(value = "The fraction collection, ex [0.5,0.5]", required = true)
	private List<Double> fractions;

	public ProfileTO() {
	}

	public ProfileTO(String profile, List<Double> fractions) {
		this.profile = profile;
		this.fractions = fractions;
	}

	public String getProfile() {
		return profile;
	}

	public void setProfile(String profile) {
		this.profile = profile;
	}

	public List<Double> getFractions() {
		return fractions;
	}

	public void setFractions(List<Double> fractions) {
		this.fractions = fractions;
	}

	@Override
	public String toString() {
		return "ProfileTO{" +
				"profile='" + profile + '\'' +
				", fractions=" + fractions +
				'}';
	}
}
