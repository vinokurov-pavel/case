--changeset pv:1

CREATE TABLE profile (
  profile   VARCHAR(10)      NOT NULL,
  month     VARCHAR(3)       NOT NULL,
  fractions NUMERIC(2) ARRAY NOT NULL,
  PRIMARY KEY (profile),
  UNIQUE (profile, month)
);

CREATE TABLE readings (
  connection_id VARCHAR(4)  NOT NULL,
  profile       VARCHAR(10) NOT NULL,
  reading_list  INT ARRAY   NOT NULL,
  PRIMARY KEY (connection_id)
);