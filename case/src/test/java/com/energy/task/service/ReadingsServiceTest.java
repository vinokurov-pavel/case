package com.energy.task.service;

import com.energy.task.BaseTest;
import com.energy.task.dao.ReadingsRepository;
import com.energy.task.model.Readings;
import com.energy.task.service.validation.ReadingValidator;
import com.energy.task.service.validation.ValidationResult;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Arrays;

import static org.mockito.BDDMockito.*;

/**
 * The number of integration tests using REST api.
 */
public class ReadingsServiceTest extends BaseTest {

	@Autowired
	@Qualifier("readingsService")
	ReadingsService readingsService;

	@MockBean
	@Qualifier("readingValidator")
	ReadingValidator readingValidator;

	@MockBean
	@Qualifier("readingsRepository")
	ReadingsRepository readingsRepository;

	@Test
	public void testSaveValid() {
		Readings valid = new Readings("0001", "A", Arrays.asList(30, 60));
		when(readingValidator.validate(same(valid))).thenReturn(ValidationResult.valid());
		readingsService.save(valid);
		verify(readingsRepository).save(same(valid));

	}

	@Test
	public void testSaveInvalid() {
		Readings invalid = new Readings("0002", "B", Arrays.asList(60, 100));
		when(readingValidator.validate(same(invalid))).thenReturn(ValidationResult.invalid(ValidationResult.ValidationError.PROFILE_NOT_FOUND));
		readingsService.save(invalid);
		verify(readingsRepository, never()).save(same(invalid));
	}

}
