package com.energy.task.web;

import com.energy.task.BaseTest;
import com.energy.task.transport.ProfileTO;
import com.energy.task.transport.ReadingsTO;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;

import java.util.Arrays;

/**
 * The number of integration tests via REST api.
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class RestIntegrationTests extends BaseTest {

	private static String PROFILE_A = "A";
	private static String CONN_1 = "0001";
	@Autowired
	private TestRestTemplate restTemplate;

	/**
	 * Valid profile
	 */
	private ProfileTO validProfile = new ProfileTO(PROFILE_A, Arrays.asList(0.01, 0.1, 0.1, 0.05, 0.05, 0.05, 0.05, 0.1, 0.1, 0.1, 0.1, 0.19));

	/**
	 * Invalid profile
	 */
	private ProfileTO invalidProfile = new ProfileTO(PROFILE_A, Arrays.asList(0.01, 0.1, 0.1, 0.05, 0.05, 0.05, 0.05, 0.1, 0.1, 0.1, 0.1, 0.9));//sum!=1.0

	/**
	 * Valid Reading
	 */
	private ReadingsTO validReading = new ReadingsTO(CONN_1, PROFILE_A, Arrays.asList(1, 11, 21, 26, 31, 36, 41, 51, 61, 71, 81, 100));

	@Test
	public void testSaveInvalidData() {

		//Send invalid profile
		ResponseEntity<Object> saveResponse = restTemplate.exchange(
				"/api/profile/save",
				HttpMethod.POST,
				new HttpEntity<>(invalidProfile),
				new ParameterizedTypeReference<Object>() {
				}
		);
		Assert.assertTrue("Expected 400 error", saveResponse.getStatusCode().is4xxClientError());
	}

	@Test
	public void testProfileCrudOperations() {
		//Send valid fractions
		ResponseEntity<Object> saveResponse = restTemplate.exchange(
				"/api/profile/save",
				HttpMethod.POST,
				new HttpEntity<>(validProfile),
				new ParameterizedTypeReference<Object>() {
				}
		);
		Assert.assertEquals(HttpStatus.OK, saveResponse.getStatusCode());

		//Get all fractions by profile
		ResponseEntity<ProfileTO> getResponse = restTemplate.exchange("/api/profile/get/{profile}", HttpMethod.GET, null, ProfileTO.class, PROFILE_A);
		Assert.assertEquals(HttpStatus.OK, getResponse.getStatusCode());

		//Check not empty result
		ProfileTO profileTO = getResponse.getBody();
		Assert.assertNotNull("'Profile' field can't be null", profileTO.getProfile());

		//Delete all fractions by profile
		ResponseEntity<Object> deleteResponse = restTemplate.exchange(
				"/api/profile/delete/{profile}",
				HttpMethod.POST,
				null,
				new ParameterizedTypeReference<Object>() {
				},
				PROFILE_A
		);
		Assert.assertEquals(HttpStatus.OK, deleteResponse.getStatusCode());

		//Check all fractions is removed
		ResponseEntity<ProfileTO> emptyResponse = restTemplate.exchange("/api/profile/get/{profile}", HttpMethod.GET, null, ProfileTO.class, PROFILE_A);
		Assert.assertEquals(HttpStatus.NOT_FOUND, emptyResponse.getStatusCode());
	}

	@Test
	public void testReadingsCrudOperations() {
		//Send valid fractions
		restTemplate.postForLocation("/api/profile/save", validProfile);

		ResponseEntity<Object> saveResponse = restTemplate.exchange(
				"/api/readings/save",
				HttpMethod.POST,
				new HttpEntity<>(validReading),
				new ParameterizedTypeReference<Object>() {
				}
		);
		Assert.assertEquals(HttpStatus.OK, saveResponse.getStatusCode());

		//Get all metrics by profile 'A'
		ResponseEntity<ReadingsTO> savedMetricsResponse = restTemplate.exchange("/api/readings/get/{profile}", HttpMethod.GET, null, ReadingsTO.class, CONN_1);
		Assert.assertEquals(HttpStatus.OK, savedMetricsResponse.getStatusCode());

		//Check not empty result
		ReadingsTO readingsTO = savedMetricsResponse.getBody();
		Assert.assertNotNull("Response body can't be null", readingsTO);
		Assert.assertNotNull("'Connection Id' can't be null", readingsTO.getConnectionId());
		Assert.assertNotNull("'Profile' can't be null", readingsTO.getProfile());
		Assert.assertEquals(12, readingsTO.getValues().size());

		//Delete all metrics by profile 'A'
		ResponseEntity<Object> deleteResponse = restTemplate.exchange(
				"/api/readings/delete/{profile}",
				HttpMethod.POST,
				null,
				new ParameterizedTypeReference<Object>() {
				},
				CONN_1
		);
		Assert.assertEquals(HttpStatus.OK, deleteResponse.getStatusCode());

		//Get all metrics by profile 'A'(already deleted)
		ResponseEntity<ReadingsTO> emptyResponse = restTemplate.exchange("/api/readings/get/{profile}", HttpMethod.GET, null, ReadingsTO.class, CONN_1);
		Assert.assertEquals(HttpStatus.NOT_FOUND, emptyResponse.getStatusCode());
	}
}
